// Função de Mudar estilo da NAV de acordo com Scroll
window.onscroll = function () {
  var scroll = this.scrollY;
  const nav = document.querySelector("nav");

  if (scroll != 0) {
    nav.classList.add("nav-scrolled")
  }
  else {
    nav.classList.remove("nav-scrolled")
  }
}

// Função para dar Scroll para onde necessário ( só útilizada para subir quando clicado em Home)
var aux = 0;
function fun_name(desce) {
  var elem = document.getElementById(desce);
  elem.scrollIntoView();

}

// Função para mover itens

var dragItem = document.querySelector("#move");
var container = document.querySelector("#move");

var active = false;
var currentX;
var currentY;
var initialX;
var initialY;
var xOffset = 0;
var yOffset = 0;

container.addEventListener("touchstart", dragStart, false);
container.addEventListener("touchend", dragEnd, false);
container.addEventListener("touchmove", drag, false);

container.addEventListener("mousedown", dragStart, false);
container.addEventListener("mouseup", dragEnd, false);
container.addEventListener("mousemove", drag, false);

function dragStart(e) {
  if (e.type === "touchstart") {
    initialX = e.touches[0].clientX - xOffset;
    initialY = e.touches[0].clientY - yOffset;
  } else {
    initialX = e.clientX - xOffset;
    initialY = e.clientY - yOffset;
  }

  if (e.target === dragItem) {
    active = true;
  }
}

function dragEnd(e) {
  initialX = currentX;
  initialY = currentY;

  active = false;
}

function drag(e) {
  if (active) {

    e.preventDefault();

    if (e.type === "touchmove") {
      currentX = e.touches[0].clientX - initialX;
      currentY = e.touches[0].clientY - initialY;
    } else {
      currentX = e.clientX - initialX;
      currentY = e.clientY - initialY;
    }

    xOffset = currentX;
    yOffset = currentY;

    setTranslate(currentX, currentY, dragItem);
  }
}

function setTranslate(xPos, yPos, el) {
  el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
}
